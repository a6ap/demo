/********** GENERATED on 09/05/2021 at 08:55:02 by CB9980000028**************/
 @OData.entitySet.name: 'I_IsPotentialFutureIssText' 
 @OData.entityType.name: 'I_IsPotentialFutureIssTextType' 
 define root abstract entity ZI_ISPOTENTIALFUTURE51444736E9 { 
 key IsPotentialFutureIssue : abap.char( 1 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'IsPotentialFutureIssueDesc_vc' 
 IsPotentialFutureIssueDesc : abap.char( 60 ) ; 
 IsPotentialFutureIssueDesc_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
