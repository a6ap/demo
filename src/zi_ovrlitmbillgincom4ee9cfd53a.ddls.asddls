/********** GENERATED on 09/05/2021 at 08:55:05 by CB9980000028**************/
 @OData.entitySet.name: 'I_OvrlItmBillgIncompltnStsText' 
 @OData.entityType.name: 'I_OvrlItmBillgIncompltnStsTextType' 
 define root abstract entity ZI_OVRLITMBILLGINCOM4EE9CFD53A { 
 key OvrlItmBillingIncompletionSts : abap.char( 1 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'OvrlItmBillgIncompltnStsDes_vc' 
 OvrlItmBillgIncompltnStsDesc : abap.char( 20 ) ; 
 OvrlItmBillgIncompltnStsDes_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
