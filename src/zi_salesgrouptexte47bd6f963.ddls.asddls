/********** GENERATED on 09/05/2021 at 08:55:08 by CB9980000028**************/
 @OData.entitySet.name: 'I_SalesGroupText' 
 @OData.entityType.name: 'I_SalesGroupTextType' 
 define root abstract entity ZI_SALESGROUPTEXTE47BD6F963 { 
 key SalesGroup : abap.char( 3 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'SalesGroupName_vc' 
 SalesGroupName : abap.char( 20 ) ; 
 SalesGroupName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
