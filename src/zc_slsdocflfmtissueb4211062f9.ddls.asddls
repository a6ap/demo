/********** GENERATED on 09/05/2021 at 08:55:53 by CB9980000028**************/
 @OData.entitySet.name: 'C_SlsDocFlfmtIssue' 
 @OData.entityType.name: 'C_SlsDocFlfmtIssueType' 

/*+[hideWarning] { "IDS" : [ "CARDINALITY_CHECK" ] }*/
 define root abstract entity ZC_SLSDOCFLFMTISSUEB4211062F9 { 
 key ID : abap.string( 0 ) ; 
 @Odata.property.valueControl: 'SalesDocument_vc' 
 SalesDocument : abap.char( 10 ) ; 
 SalesDocument_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'DeliveryDocument_vc' 
 DeliveryDocument : abap.char( 10 ) ; 
 DeliveryDocument_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'BillingDocument_vc' 
 BillingDocument : abap.char( 10 ) ; 
 BillingDocument_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'PurchasingDocument_vc' 
 PurchasingDocument : abap.char( 10 ) ; 
 PurchasingDocument_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'ManufacturingOrder_vc' 
 ManufacturingOrder : abap.char( 12 ) ; 
 ManufacturingOrder_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'Issue_vc' 
 Issue : abap.char( 4 ) ; 
 Issue_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'Issue_Text_vc' 
 Issue_Text : abap.char( 60 ) ; 
 Issue_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'IssueCategory_vc' 
 IssueCategory : abap.char( 5 ) ; 
 IssueCategory_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'IssueCategory_Text_vc' 
 IssueCategory_Text : abap.char( 60 ) ; 
 IssueCategory_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'DueDays_vc' 
 DueDays : abap.int4 ; 
 DueDays_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'DueHorizonDurationInDays_vc' 
 DueHorizonDurationInDays : abap.int4 ; 
 DueHorizonDurationInDays_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'DueDateStatus_vc' 
 DueDateStatus : abap.char( 1 ) ; 
 DueDateStatus_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'DueDateStatus_Text_vc' 
 DueDateStatus_Text : abap.char( 60 ) ; 
 DueDateStatus_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'IsPotentialFutureIssue_vc' 
 IsPotentialFutureIssue : abap.char( 1 ) ; 
 IsPotentialFutureIssue_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'IsPotentialFutureIssue_Text_vc' 
 IsPotentialFutureIssue_Text : abap.char( 60 ) ; 
 IsPotentialFutureIssue_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SemanticObject_vc' 
 SemanticObject : abap.char( 23 ) ; 
 SemanticObject_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'ManufacturingObject_vc' 
 ManufacturingObject : abap.char( 15 ) ; 
 ManufacturingObject_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'NmbrOfIssuesInOrder_vc' 
 NmbrOfIssuesInOrder : abap.int4 ; 
 NmbrOfIssuesInOrder_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'NmbrOfIssuesInDelivery_vc' 
 NmbrOfIssuesInDelivery : abap.int4 ; 
 NmbrOfIssuesInDelivery_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'NmbrOfIssuesInInvoice_vc' 
 NmbrOfIssuesInInvoice : abap.int4 ; 
 NmbrOfIssuesInInvoice_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'NmbrOfIssuesInSupply_vc' 
 NmbrOfIssuesInSupply : abap.int4 ; 
 NmbrOfIssuesInSupply_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'NmbrOfAllIssues_vc' 
 NmbrOfAllIssues : abap.int4 ; 
 NmbrOfAllIssues_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'NmbrOfItemsInOrder_vc' 
 NmbrOfItemsInOrder : abap.int4 ; 
 NmbrOfItemsInOrder_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesDocumentType_vc' 
 SalesDocumentType : abap.char( 4 ) ; 
 SalesDocumentType_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesDocumentType_Text_vc' 
 SalesDocumentType_Text : abap.char( 20 ) ; 
 SalesDocumentType_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SDDocumentCategory_vc' 
 SDDocumentCategory : abap.char( 4 ) ; 
 SDDocumentCategory_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SDDocumentCategory_Text_vc' 
 SDDocumentCategory_Text : abap.char( 60 ) ; 
 SDDocumentCategory_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CreatedByUser_vc' 
 CreatedByUser : abap.char( 12 ) ; 
 CreatedByUser_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CreatedByUserName_vc' 
 CreatedByUserName : abap.char( 80 ) ; 
 CreatedByUserName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesOrganization_vc' 
 SalesOrganization : abap.char( 4 ) ; 
 SalesOrganization_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesOrganization_Text_vc' 
 SalesOrganization_Text : abap.char( 20 ) ; 
 SalesOrganization_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'DistributionChannel_vc' 
 DistributionChannel : abap.char( 2 ) ; 
 DistributionChannel_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'DistributionChannel_Text_vc' 
 DistributionChannel_Text : abap.char( 20 ) ; 
 DistributionChannel_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OrganizationDivision_vc' 
 OrganizationDivision : abap.char( 2 ) ; 
 OrganizationDivision_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OrganizationDivision_Text_vc' 
 OrganizationDivision_Text : abap.char( 20 ) ; 
 OrganizationDivision_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesGroup_vc' 
 SalesGroup : abap.char( 3 ) ; 
 SalesGroup_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesGroup_Text_vc' 
 SalesGroup_Text : abap.char( 20 ) ; 
 SalesGroup_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesOffice_vc' 
 SalesOffice : abap.char( 4 ) ; 
 SalesOffice_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesOffice_Text_vc' 
 SalesOffice_Text : abap.char( 20 ) ; 
 SalesOffice_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SoldToParty_vc' 
 SoldToParty : abap.char( 10 ) ; 
 SoldToParty_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SoldToPartyName_vc' 
 SoldToPartyName : abap.char( 80 ) ; 
 SoldToPartyName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'ShipToParty_vc' 
 ShipToParty : abap.char( 10 ) ; 
 ShipToParty_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'ShipToPartyName_vc' 
 ShipToPartyName : abap.char( 80 ) ; 
 ShipToPartyName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'ResponsibleEmployee_vc' 
 ResponsibleEmployee : abap.numc( 8 ) ; 
 ResponsibleEmployee_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'ResponsibleEmployeeName_vc' 
 ResponsibleEmployeeName : abap.char( 80 ) ; 
 ResponsibleEmployeeName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'PayerParty_vc' 
 PayerParty : abap.char( 10 ) ; 
 PayerParty_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'PayerPartyName_vc' 
 PayerPartyName : abap.char( 80 ) ; 
 PayerPartyName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CustomerClassification_vc' 
 CustomerClassification : abap.char( 2 ) ; 
 CustomerClassification_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CustomerClassification_Text_vc' 
 CustomerClassification_Text : abap.char( 20 ) ; 
 CustomerClassification_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesDocumentDate_vc' 
 SalesDocumentDate : RAP_CP_ODATA_V2_EDM_DATETIME ; 
 SalesDocumentDate_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CreditSegment_vc' 
 CreditSegment : abap.char( 10 ) ; 
 CreditSegment_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CreditSegment_Text_vc' 
 CreditSegment_Text : abap.char( 50 ) ; 
 CreditSegment_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'PurchaseOrderByCustomer_vc' 
 PurchaseOrderByCustomer : abap.char( 35 ) ; 
 PurchaseOrderByCustomer_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'TotalNetAmount_vc' 
 @Semantics.amount.currencyCode: 'TransactionCurrency' 
 TotalNetAmount : abap.curr( 16, 3 ) ; 
 TotalNetAmount_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'TransactionCurrency_vc' 
 @Semantics.currencyCode: true 
 TransactionCurrency : abap.cuky ; 
 TransactionCurrency_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'TotalNetAmountInStatisticsC_vc' 
 @Semantics.amount.currencyCode: 'StatisticsCurrency' 
 TotalNetAmountInStatisticsCrcy : abap.curr( 16, 3 ) ; 
 TotalNetAmountInStatisticsC_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'StatisticsCurrency_vc' 
 @Semantics.currencyCode: true 
 StatisticsCurrency : abap.cuky ; 
 StatisticsCurrency_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'RequestedDeliveryDate_vc' 
 RequestedDeliveryDate : RAP_CP_ODATA_V2_EDM_DATETIME ; 
 RequestedDeliveryDate_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'ReqdDelivDateYearWeek_vc' 
 ReqdDelivDateYearWeek : abap.numc( 6 ) ; 
 ReqdDelivDateYearWeek_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'DeliveryBlockReason_vc' 
 DeliveryBlockReason : abap.char( 2 ) ; 
 DeliveryBlockReason_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'DeliveryBlockReason_Text_vc' 
 DeliveryBlockReason_Text : abap.char( 20 ) ; 
 DeliveryBlockReason_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'HeaderBillingBlockReason_vc' 
 HeaderBillingBlockReason : abap.char( 2 ) ; 
 HeaderBillingBlockReason_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'HeaderBillingBlockReason_Te_vc' 
 HeaderBillingBlockReason_Text : abap.char( 20 ) ; 
 HeaderBillingBlockReason_Te_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesDocumentRjcnReason_vc' 
 SalesDocumentRjcnReason : abap.char( 2 ) ; 
 SalesDocumentRjcnReason_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'HdrGeneralIncompletionStatu_vc' 
 HdrGeneralIncompletionStatus : abap.char( 1 ) ; 
 HdrGeneralIncompletionStatu_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'HdrGeneralIncompletionStatu_v1' 
 @OData.property.name: 'HdrGeneralIncompletionStatus_Text' 
 HdrGeneralIncompletionStatus_1 : abap.char( 20 ) ; 
 HdrGeneralIncompletionStatu_v1 : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'HeaderDelivIncompletionStat_vc' 
 HeaderDelivIncompletionStatus : abap.char( 1 ) ; 
 HeaderDelivIncompletionStat_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'HeaderDelivIncompletionStat_v1' 
 @OData.property.name: 'HeaderDelivIncompletionStatus_Text' 
 HeaderDelivIncompletionStatus1 : abap.char( 20 ) ; 
 HeaderDelivIncompletionStat_v1 : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OverallPricingIncompletionS_vc' 
 OverallPricingIncompletionSts : abap.char( 1 ) ; 
 OverallPricingIncompletionS_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OverallPricingIncompletionS_v1' 
 @OData.property.name: 'OverallPricingIncompletionSts_Text' 
 OverallPricingIncompletionSts1 : abap.char( 20 ) ; 
 OverallPricingIncompletionS_v1 : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'HeaderBillgIncompletionStat_vc' 
 HeaderBillgIncompletionStatus : abap.char( 1 ) ; 
 HeaderBillgIncompletionStat_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'HeaderBillgIncompletionStat_v1' 
 @OData.property.name: 'HeaderBillgIncompletionStatus_Text' 
 HeaderBillgIncompletionStatus1 : abap.char( 20 ) ; 
 HeaderBillgIncompletionStat_v1 : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OvrlItmGeneralIncompletionS_vc' 
 OvrlItmGeneralIncompletionSts : abap.char( 1 ) ; 
 OvrlItmGeneralIncompletionS_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OvrlItmGeneralIncompletionS_v1' 
 @OData.property.name: 'OvrlItmGeneralIncompletionSts_Text' 
 OvrlItmGeneralIncompletionSts1 : abap.char( 20 ) ; 
 OvrlItmGeneralIncompletionS_v1 : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OvrlItmBillingIncompletionS_vc' 
 OvrlItmBillingIncompletionSts : abap.char( 1 ) ; 
 OvrlItmBillingIncompletionS_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OvrlItmBillingIncompletionS_v1' 
 @OData.property.name: 'OvrlItmBillingIncompletionSts_Text' 
 OvrlItmBillingIncompletionSts1 : abap.char( 20 ) ; 
 OvrlItmBillingIncompletionS_v1 : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OvrlItmDelivIncompletionSts_vc' 
 OvrlItmDelivIncompletionSts : abap.char( 1 ) ; 
 OvrlItmDelivIncompletionSts_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OvrlItmDelivIncompletionSts_v1' 
 @OData.property.name: 'OvrlItmDelivIncompletionSts_Text' 
 OvrlItmDelivIncompletionSts_T1 : abap.char( 20 ) ; 
 OvrlItmDelivIncompletionSts_v1 : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'PaytAuthsnCreditCheckSts_vc' 
 PaytAuthsnCreditCheckSts : abap.char( 1 ) ; 
 PaytAuthsnCreditCheckSts_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'PaytAuthsnCreditCheckSts_Te_vc' 
 PaytAuthsnCreditCheckSts_Text : abap.char( 20 ) ; 
 PaytAuthsnCreditCheckSts_Te_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CustomerProject_vc' 
 CustomerProject : abap.char( 40 ) ; 
 CustomerProject_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CustomerProjectName_vc' 
 CustomerProjectName : abap.char( 40 ) ; 
 CustomerProjectName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OrganizationBPName1_vc' 
 OrganizationBPName1 : abap.char( 35 ) ; 
 OrganizationBPName1_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OrganizationBPName2_vc' 
 OrganizationBPName2 : abap.char( 35 ) ; 
 OrganizationBPName2_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 @OData.property.name: 'to_BillingBlockReasonText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _BillingBlockReasonText : association [0..*] to ZI_BILLINGBLOCKREASO1D2F20BE82 on 1 = 1; 
 @OData.property.name: 'to_CreditSegment' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _CreditSegment : association [1] to ZI_CREDITMANAGEMENTS21100B86E0 on 1 = 1; 
 @OData.property.name: 'to_CreditSegmentText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _CreditSegmentText : association [0..*] to ZI_CREDITMANAGEMENTS1F25D9CF4C on 1 = 1; 
 @OData.property.name: 'to_CustomerClassification' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _CustomerClassification : association [1] to ZI_CUSTOMERCLASSIFICE0F4AA7909 on 1 = 1; 
 @OData.property.name: 'to_CustomerClassificationText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _CustomerClassificationText : association [0..*] to ZI_CUSTOMERCLASSIFIC9317EE9068 on 1 = 1; 
 @OData.property.name: 'to_DeliveryBlockReason' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _DeliveryBlockReason : association [1] to ZI_DELIVERYBLOCKREAS1C83EE3E7D on 1 = 1; 
 @OData.property.name: 'to_DeliveryBlockReasonText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _DeliveryBlockReasonText : association [0..*] to ZI_DELIVERYBLOCKREASFF66B3798F on 1 = 1; 
 @OData.property.name: 'to_DistributionChannelText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _DistributionChannelText : association [0..*] to ZI_DISTRIBUTIONCHANNBA9D4250E8 on 1 = 1; 
 @OData.property.name: 'to_DistributionChannelVH' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _DistributionChannelVH : association [1] to ZC_DISCHANNELVALUEHE3730C81565 on 1 = 1; 
 @OData.property.name: 'to_DivisionText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _DivisionText : association [0..*] to ZI_DIVISIONTEXTDD2A3FE062 on 1 = 1; 
 @OData.property.name: 'to_DueDateStatus' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _DueDateStatus : association [1] to ZI_DUEDATESTATUS25B71FA332 on 1 = 1; 
 @OData.property.name: 'to_DueDateStatusText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _DueDateStatusText : association [0..*] to ZI_DUEDATESTATUSTEXT40D20796BD on 1 = 1; 
 @OData.property.name: 'to_HdrBillgIncompletionStatusT' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _HdrBillgIncompletionStatusT : association [0..*] to ZI_HDRBILLGINCOMPLET5FC294CBA1 on 1 = 1; 
 @OData.property.name: 'to_HdrDelivIncompletionStatusT' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _HdrDelivIncompletionStatusT : association [0..*] to ZI_HDRDELIVINCOMPLET76293BFECB on 1 = 1; 
 @OData.property.name: 'to_HdrGenIncompletionStatusT' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _HdrGenIncompletionStatusT : association [0..*] to ZI_HDRGENINCOMPLETIO209EBDE7CA on 1 = 1; 
 @OData.property.name: 'to_HdrGeneralIncompletionStatus' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _HdrGeneralIncompletionStatus : association [1] to ZI_HDRGENINCOMPLETIOF7ED06ADA1 on 1 = 1; 
 @OData.property.name: 'to_HeaderBillgIncompletionStatus' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _HeaderBillgIncompletionStatus : association [1] to ZI_HDRBILLGINCOMPLET913A6FBF10 on 1 = 1; 
 @OData.property.name: 'to_HeaderBillingBlockReason' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _HeaderBillingBlockReason : association [1] to ZI_BILLINGBLOCKREASOF928C1E014 on 1 = 1; 
 @OData.property.name: 'to_HeaderDelivIncompletionStatus' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _HeaderDelivIncompletionStatus : association [1] to ZI_HDRDELIVINCOMPLET6F58E5CFF1 on 1 = 1; 
 @OData.property.name: 'to_IsPotentialFutureIssText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _IsPotentialFutureIssText : association [0..*] to ZI_ISPOTENTIALFUTURE51444736E9 on 1 = 1; 
 @OData.property.name: 'to_IsPotentialFutureIssue' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _IsPotentialFutureIssue : association [1] to ZI_ISPOTENTIALFUTUREC81EAE9897 on 1 = 1; 
 @OData.property.name: 'to_Issue' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _Issue : association [1] to ZI_ISSUE437DCF3235 on 1 = 1; 
 @OData.property.name: 'to_IssueCategory' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _IssueCategory : association [1] to ZI_ISSUECATEGORY456A9F0DAD on 1 = 1; 
 @OData.property.name: 'to_IssueCategoryText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _IssueCategoryText : association [0..*] to ZI_ISSUECATEGORYTEXT915066CF63 on 1 = 1; 
 @OData.property.name: 'to_IssueText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _IssueText : association [0..*] to ZI_ISSUETEXTA7C7D6E6C2 on 1 = 1; 
 @OData.property.name: 'to_OrganizationDivisionVH' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _OrganizationDivisionVH : association [1] to ZC_ORGDIVISIONVALUEH37D1858C92 on 1 = 1; 
 @OData.property.name: 'to_OverallPrcIncompletionStsT' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _OverallPrcIncompletionStsT : association [0..*] to ZI_OVERALLPRCINCOMPL6F58F45B22 on 1 = 1; 
 @OData.property.name: 'to_OverallPricingIncompletionSts' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _OverallPricingIncompletionSts : association [1] to ZI_OVERALLPRCINCOMPL783455936B on 1 = 1; 
 @OData.property.name: 'to_OvrlItmBillgIncompltnStsT' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _OvrlItmBillgIncompltnStsT : association [0..*] to ZI_OVRLITMBILLGINCOM4EE9CFD53A on 1 = 1; 
 @OData.property.name: 'to_OvrlItmBillingIncompletionSts' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _OvrlItmBillingIncompletionSts : association [1] to ZI_OVRLITMBILLGINCOM8BF143A296 on 1 = 1; 
 @OData.property.name: 'to_OvrlItmDelivIncompletionSts' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _OvrlItmDelivIncompletionSts : association [1] to ZI_OVRLITMDELIVINCOM70D38EE9C7 on 1 = 1; 
 @OData.property.name: 'to_OvrlItmDelivIncompletionStsT' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _OvrlItmDelivIncompletionStsT : association [0..*] to ZI_OVRLITMDELIVINCOM132FBA0E55 on 1 = 1; 
 @OData.property.name: 'to_OvrlItmGenIncompletionStsT' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _OvrlItmGenIncompletionStsT : association [0..*] to ZI_OVRLITMGENINCOMPL0ACE41D465 on 1 = 1; 
 @OData.property.name: 'to_OvrlItmGeneralIncompletionSts' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _OvrlItmGeneralIncompletionSts : association [1] to ZI_OVRLITMGENINCOMPL7CF2A945AF on 1 = 1; 
 @OData.property.name: 'to_PayerParty' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _PayerParty : association [1] to ZI_CUSTOMERA77B8DD617 on 1 = 1; 
 @OData.property.name: 'to_PaytAuthsnCreditCheckSts' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _PaytAuthsnCreditCheckSts : association [1] to ZI_PAYTAUTHSNCREDITCEB2C6E18B0 on 1 = 1; 
 @OData.property.name: 'to_PaytAuthsnCreditCheckStsText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _PaytAuthsnCreditCheckStsText : association [0..*] to ZI_PAYTAUTHSNCREDITCAD632EB5F0 on 1 = 1; 
 @OData.property.name: 'to_ResponsibleEmployee' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _ResponsibleEmployee : association [1] to ZI_EMPLOYEE70D631E645 on 1 = 1; 
 @OData.property.name: 'to_SDDocumentCategory' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SDDocumentCategory : association [1] to ZI_SDDOCUMENTCATEGORFACA8E170A on 1 = 1; 
 @OData.property.name: 'to_SDDocumentCategoryText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SDDocumentCategoryText : association [0..*] to ZI_SDDOCUMENTCATEGOR2322424F76 on 1 = 1; 
 @OData.property.name: 'to_SalesDocumentRjcnReason' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SalesDocumentRjcnReason : association [1] to ZI_SALESDOCUMENTRJCN65C0D3DDB5 on 1 = 1; 
 @OData.property.name: 'to_SalesDocumentType' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SalesDocumentType : association [1] to ZI_SALESDOCUMENTTYPEE13774EFBE on 1 = 1; 
 @OData.property.name: 'to_SalesDocumentTypeText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SalesDocumentTypeText : association [0..*] to ZI_SALESDOCUMENTTYPEADD530B1C0 on 1 = 1; 
 @OData.property.name: 'to_SalesGroupText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SalesGroupText : association [0..*] to ZI_SALESGROUPTEXTE47BD6F963 on 1 = 1; 
 @OData.property.name: 'to_SalesGroupVH' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SalesGroupVH : association [1] to ZC_SALESGROUPVALUEHE30F8775826 on 1 = 1; 
 @OData.property.name: 'to_SalesOfficeText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SalesOfficeText : association [0..*] to ZI_SALESOFFICETEXTB8E134E93E on 1 = 1; 
 @OData.property.name: 'to_SalesOfficeVH' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SalesOfficeVH : association [1] to ZC_SALESOFFICEVALUEH510468EAF3 on 1 = 1; 
 @OData.property.name: 'to_SalesOrganization' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SalesOrganization : association [1] to ZI_SALESORGANIZATIONE83176B9E1 on 1 = 1; 
 @OData.property.name: 'to_SalesOrganizationText' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _SalesOrganizationText : association [0..*] to ZI_SALESORGANIZATION2DCE9FCF26 on 1 = 1; 
 @OData.property.name: 'to_ShipToParty' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _ShipToParty : association [1] to ZI_CUSTOMERA77B8DD617 on 1 = 1; 
 @OData.property.name: 'to_StatisticsCurrency' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _StatisticsCurrency : association [1] to ZI_CURRENCYE5F35E6075 on 1 = 1; 
 @OData.property.name: 'to_TransactionCurrency' 
//A dummy on-condition is required for associations in abstract entities 
//On-condition is not relevant for runtime 
 _TransactionCurrency : association [1] to ZI_CURRENCYE5F35E6075 on 1 = 1; 
 } 
