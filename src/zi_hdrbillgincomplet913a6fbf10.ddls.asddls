/********** GENERATED on 09/05/2021 at 08:55:00 by CB9980000028**************/
 @OData.entitySet.name: 'I_HdrBillgIncompletionStatus' 
 @OData.entityType.name: 'I_HdrBillgIncompletionStatusType' 
 define root abstract entity ZI_HDRBILLGINCOMPLET913A6FBF10 { 
 key HeaderBillgIncompletionStatus : abap.char( 1 ) ; 
 @Odata.property.valueControl: 'HeaderBillgIncompletionStat_vc' 
 @OData.property.name: 'HeaderBillgIncompletionStatus_Text' 
 HeaderBillgIncompletionStatus1 : abap.char( 20 ) ; 
 HeaderBillgIncompletionStat_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
