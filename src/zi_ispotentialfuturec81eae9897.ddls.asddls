/********** GENERATED on 09/05/2021 at 08:55:03 by CB9980000028**************/
 @OData.entitySet.name: 'I_IsPotentialFutureIssue' 
 @OData.entityType.name: 'I_IsPotentialFutureIssueType' 
 define root abstract entity ZI_ISPOTENTIALFUTUREC81EAE9897 { 
 key IsPotentialFutureIssue : abap.char( 1 ) ; 
 @Odata.property.valueControl: 'IsPotentialFutureIssue_Text_vc' 
 IsPotentialFutureIssue_Text : abap.char( 60 ) ; 
 IsPotentialFutureIssue_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
