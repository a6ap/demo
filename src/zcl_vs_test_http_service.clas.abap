CLASS zcl_vs_test_http_service DEFINITION
  PUBLIC
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_http_service_extension .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS zcl_vs_test_http_service IMPLEMENTATION.


  METHOD if_http_service_extension~handle_request.
    CASE request->get_method(  ).
      WHEN CONV string( if_web_http_client=>get ).
        DATA(lt_params) = request->get_form_fields(  ).
        DATA(ls_user) = lt_params[ name = 'user' ].
        response->set_text( |Hello | && ls_user-value && | | && cl_abap_context_info=>get_user_technical_name(  ) ).
      WHEN OTHERS.
        response->set_status( 404 ).
    ENDCASE.
  ENDMETHOD.
ENDCLASS.
