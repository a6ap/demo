/********** GENERATED on 09/05/2021 at 08:54:55 by CB9980000028**************/
 @OData.entitySet.name: 'I_CreditManagementSegmentTxt' 
 @OData.entityType.name: 'I_CreditManagementSegmentTxtType' 
 define root abstract entity ZI_CREDITMANAGEMENTS1F25D9CF4C { 
 key CreditSegment : abap.char( 10 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'CreditSegmentName_vc' 
 CreditSegmentName : abap.char( 50 ) ; 
 CreditSegmentName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
