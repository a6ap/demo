/********** GENERATED on 09/05/2021 at 08:55:01 by CB9980000028**************/
 @OData.entitySet.name: 'I_HdrDelivIncompletionStatusT' 
 @OData.entityType.name: 'I_HdrDelivIncompletionStatusTType' 
 define root abstract entity ZI_HDRDELIVINCOMPLET76293BFECB { 
 key HeaderDelivIncompletionStatus : abap.char( 1 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'HdrDelivIncompletionStatusD_vc' 
 HdrDelivIncompletionStatusDesc : abap.char( 20 ) ; 
 HdrDelivIncompletionStatusD_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
