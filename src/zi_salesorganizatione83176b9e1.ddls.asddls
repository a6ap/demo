/********** GENERATED on 09/05/2021 at 08:55:09 by CB9980000028**************/
 @OData.entitySet.name: 'I_SalesOrganization' 
 @OData.entityType.name: 'I_SalesOrganizationType' 
 define root abstract entity ZI_SALESORGANIZATIONE83176B9E1 { 
 key SalesOrganization : abap.char( 4 ) ; 
 @Odata.property.valueControl: 'SalesOrganization_Text_vc' 
 SalesOrganization_Text : abap.char( 20 ) ; 
 SalesOrganization_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesOrganizationCurrency_vc' 
 SalesOrganizationCurrency : abap.char( 5 ) ; 
 SalesOrganizationCurrency_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CompanyCode_vc' 
 CompanyCode : abap.char( 4 ) ; 
 CompanyCode_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'IntercompanyBillingCustomer_vc' 
 IntercompanyBillingCustomer : abap.char( 10 ) ; 
 IntercompanyBillingCustomer_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
