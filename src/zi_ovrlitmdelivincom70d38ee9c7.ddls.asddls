/********** GENERATED on 09/05/2021 at 08:55:06 by CB9980000028**************/
 @OData.entitySet.name: 'I_OvrlItmDelivIncompletionSts' 
 @OData.entityType.name: 'I_OvrlItmDelivIncompletionStsType' 
 define root abstract entity ZI_OVRLITMDELIVINCOM70D38EE9C7 { 
 key OvrlItmDelivIncompletionSts : abap.char( 1 ) ; 
 @Odata.property.valueControl: 'OvrlItmDelivIncompletionSts_vc' 
 @OData.property.name: 'OvrlItmDelivIncompletionSts_Text' 
 OvrlItmDelivIncompletionSts_T1 : abap.char( 20 ) ; 
 OvrlItmDelivIncompletionSts_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
