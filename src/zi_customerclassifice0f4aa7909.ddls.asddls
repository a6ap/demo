/********** GENERATED on 09/05/2021 at 08:54:56 by CB9980000028**************/
 @OData.entitySet.name: 'I_CustomerClassification' 
 @OData.entityType.name: 'I_CustomerClassificationType' 
 define root abstract entity ZI_CUSTOMERCLASSIFICE0F4AA7909 { 
 key CustomerClassification : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'CustomerClassification_Text_vc' 
 CustomerClassification_Text : abap.char( 20 ) ; 
 CustomerClassification_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
