/********** GENERATED on 09/05/2021 at 08:55:00 by CB9980000028**************/
 @OData.entitySet.name: 'I_Employee' 
 @OData.entityType.name: 'I_EmployeeType' 
 define root abstract entity ZI_EMPLOYEE70D631E645 { 
 key PersonnelNumber : abap.numc( 8 ) ; 
 @Odata.property.valueControl: 'EmployeeInternalID_vc' 
 EmployeeInternalID : abap.char( 10 ) ; 
 EmployeeInternalID_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'Employee_vc' 
 Employee : abap.char( 60 ) ; 
 Employee_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'ValidityStartDate_vc' 
 ValidityStartDate : RAP_CP_ODATA_V2_EDM_DATETIME ; 
 ValidityStartDate_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'ValidityEndDate_vc' 
 ValidityEndDate : RAP_CP_ODATA_V2_EDM_DATETIME ; 
 ValidityEndDate_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'FormOfAddress_vc' 
 FormOfAddress : abap.char( 4 ) ; 
 FormOfAddress_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'FamilyName_vc' 
 FamilyName : abap.char( 40 ) ; 
 FamilyName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'FirstName_vc' 
 FirstName : abap.char( 40 ) ; 
 FirstName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'GivenName_vc' 
 GivenName : abap.char( 40 ) ; 
 GivenName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'MiddleName_vc' 
 MiddleName : abap.char( 40 ) ; 
 MiddleName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'AdditionalFamilyName_vc' 
 AdditionalFamilyName : abap.char( 40 ) ; 
 AdditionalFamilyName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'AcademicTitle_vc' 
 AcademicTitle : abap.char( 4 ) ; 
 AcademicTitle_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'FamilyNamePrefix_vc' 
 FamilyNamePrefix : abap.char( 4 ) ; 
 FamilyNamePrefix_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'Initials_vc' 
 Initials : abap.char( 10 ) ; 
 Initials_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'FullName_vc' 
 FullName : abap.char( 80 ) ; 
 FullName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'EmployeeFullName_vc' 
 EmployeeFullName : abap.char( 80 ) ; 
 EmployeeFullName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CorrespondenceLanguage_vc' 
 CorrespondenceLanguage : abap.char( 2 ) ; 
 CorrespondenceLanguage_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'GenderCode_vc' 
 GenderCode : abap.char( 1 ) ; 
 GenderCode_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'BusinessPartnerRole_vc' 
 BusinessPartnerRole : abap.char( 6 ) ; 
 BusinessPartnerRole_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'Person_vc' 
 Person : abap.char( 10 ) ; 
 Person_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'BusinessPartnerUUID_vc' 
 BusinessPartnerUUID : sysuuid_x16 ; 
 BusinessPartnerUUID_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'BusinessUser_vc' 
 BusinessUser : abap.char( 12 ) ; 
 BusinessUser_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'UserID_vc' 
 UserID : abap.char( 12 ) ; 
 UserID_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'EmployeeImageURL_vc' 
 EmployeeImageURL : abap.char( 1000 ) ; 
 EmployeeImageURL_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CreatedByUser_vc' 
 CreatedByUser : abap.char( 12 ) ; 
 CreatedByUser_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CreationDate_vc' 
 CreationDate : RAP_CP_ODATA_V2_EDM_DATETIME ; 
 CreationDate_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CreationTime_vc' 
 CreationTime : RAP_CP_ODATA_V2_EDM_TIME ; 
 CreationTime_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'LastChangedByUser_vc' 
 LastChangedByUser : abap.char( 12 ) ; 
 LastChangedByUser_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'LastChangeDate_vc' 
 LastChangeDate : RAP_CP_ODATA_V2_EDM_DATETIME ; 
 LastChangeDate_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'LastChangeTime_vc' 
 LastChangeTime : RAP_CP_ODATA_V2_EDM_TIME ; 
 LastChangeTime_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'AuthorizationGroup_vc' 
 AuthorizationGroup : abap.char( 4 ) ; 
 AuthorizationGroup_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'IsBusinessPurposeCompleted_vc' 
 IsBusinessPurposeCompleted : abap.char( 1 ) ; 
 IsBusinessPurposeCompleted_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
