/********** GENERATED on 09/05/2021 at 08:54:54 by CB9980000028**************/
 @OData.entitySet.name: 'C_SoldToValueHelp' 
 @OData.entityType.name: 'C_SoldToValueHelpType' 
 define root abstract entity ZC_SOLDTOVALUEHELP920D7F64E1 { 
 key Customer : abap.char( 10 ) ; 
 @Odata.property.valueControl: 'CustomerName_vc' 
 CustomerName : abap.char( 80 ) ; 
 CustomerName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OrganizationBPName1_vc' 
 OrganizationBPName1 : abap.char( 35 ) ; 
 OrganizationBPName1_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OrganizationBPName2_vc' 
 OrganizationBPName2 : abap.char( 35 ) ; 
 OrganizationBPName2_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'BusinessPartnerImageURL_vc' 
 BusinessPartnerImageURL : abap.char( 255 ) ; 
 BusinessPartnerImageURL_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'PostalCode_vc' 
 PostalCode : abap.char( 10 ) ; 
 PostalCode_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CityName_vc' 
 CityName : abap.char( 40 ) ; 
 CityName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'Country_vc' 
 Country : abap.char( 3 ) ; 
 Country_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'InternationalPhoneNumber_vc' 
 InternationalPhoneNumber : abap.char( 30 ) ; 
 InternationalPhoneNumber_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'EmailAddress_vc' 
 EmailAddress : abap.char( 241 ) ; 
 EmailAddress_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
