/********** GENERATED on 09/05/2021 at 08:55:06 by CB9980000028**************/
 @OData.entitySet.name: 'I_OvrlItmGenIncompletionStsT' 
 @OData.entityType.name: 'I_OvrlItmGenIncompletionStsTType' 
 define root abstract entity ZI_OVRLITMGENINCOMPL0ACE41D465 { 
 key OvrlItmGeneralIncompletionSts : abap.char( 1 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'OvrlItmGenIncompletionStsDe_vc' 
 OvrlItmGenIncompletionStsDesc : abap.char( 20 ) ; 
 OvrlItmGenIncompletionStsDe_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
