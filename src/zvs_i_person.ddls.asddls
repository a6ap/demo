@AbapCatalog.sqlViewName: 'ZVS_I_PERSON'
@AbapCatalog.compiler.compareFilter: true
@AbapCatalog.preserveKey: true
@AccessControl.authorizationCheck: #CHECK
@EndUserText.label: 'CDS interface for Person table'
define view ZVS_CDS_I_PERSON
  as select from zvs_a_person
{
      @UI.hidden: true
  key id,

      @Search.defaultSearchElement: true
      @UI:{
        lineItem: [{position: 10, label:'Fname'}],
        identification: [ {position: 10, label: 'fname'}],
        selectionField: [ {position: 10}]
      }
      fname,

      @Search.defaultSearchElement: true
      @UI: {
        lineItem: [ {position: 20, label: 'Lname'}],
        identification: [ {position: 20, label: 'Lname'}],
        selectionField: [ {position: 20}]
      }
      lname,

      @Search.defaultSearchElement: true
      @UI: {
        lineItem: [ {position: 30, label: 'Gender'}],
        identification: [ {position: 30, label: 'Gender'}],
        selectionField: [ {position: 30}]
      }
      gender,

      @Search.defaultSearchElement: true
      @UI: {
        lineItem: [ {position: 40, label: 'Email'}],
        identification: [ {position: 40, label: 'Email'}],
        selectionField: [ {position: 40}]
      }
      email

}
