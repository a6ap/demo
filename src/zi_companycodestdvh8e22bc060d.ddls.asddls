/********** GENERATED on 09/05/2021 at 08:54:55 by CB9980000028**************/
 @OData.entitySet.name: 'I_CompanyCodeStdVH' 
 @OData.entityType.name: 'I_CompanyCodeStdVHType' 
 define root abstract entity ZI_COMPANYCODESTDVH8E22BC060D { 
 key CompanyCode : abap.char( 4 ) ; 
 @Odata.property.valueControl: 'CompanyCodeName_vc' 
 CompanyCodeName : abap.char( 25 ) ; 
 CompanyCodeName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
