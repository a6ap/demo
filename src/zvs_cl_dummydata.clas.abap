CLASS zvs_cl_dummydata DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS zvs_cl_dummydata IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    DATA: lt_person TYPE STANDARD TABLE OF zvs_a_person.

    lt_person = VALUE #(
      ( id = '7177858B537E40A79C9E29C88C1431D8' fname = 'Santa' lname = 'Claus' email = 'santaClaus@gmail.com' gender = 'M' )
      ( id = '311C7121EFE54F21BE429B88A10D4BA8' fname = 'Ded' lname = 'Moroz' email = 'dedMoroz@gmail.by' gender = 'M' )
      ( id = '066687B78C3A4C46BA57A5F419DB96ED' fname = 'Baba' lname = 'Yaga' email = 'babaYaga@gmail.by' gender = 'W' )
    ).

    DELETE FROM zvs_a_person.

    INSERT zvs_a_person FROM TABLE @lt_person.
    CLEAR lt_person.

    out->write( 'data inserted successfully!').
  ENDMETHOD.
ENDCLASS.
