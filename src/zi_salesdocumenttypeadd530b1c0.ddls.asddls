/********** GENERATED on 09/05/2021 at 08:55:08 by CB9980000028**************/
 @OData.entitySet.name: 'I_SalesDocumentTypeText' 
 @OData.entityType.name: 'I_SalesDocumentTypeTextType' 
 define root abstract entity ZI_SALESDOCUMENTTYPEADD530B1C0 { 
 key SalesDocumentType : abap.char( 4 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'SalesDocumentTypeName_vc' 
 SalesDocumentTypeName : abap.char( 20 ) ; 
 SalesDocumentTypeName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
