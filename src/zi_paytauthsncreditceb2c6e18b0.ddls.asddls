/********** GENERATED on 09/05/2021 at 08:55:07 by CB9980000028**************/
 @OData.entitySet.name: 'I_PaytAuthsnCreditCheckSts' 
 @OData.entityType.name: 'I_PaytAuthsnCreditCheckStsType' 
 define root abstract entity ZI_PAYTAUTHSNCREDITCEB2C6E18B0 { 
 key PaytAuthsnCreditCheckSts : abap.char( 1 ) ; 
 @Odata.property.valueControl: 'PaytAuthsnCreditCheckSts_Te_vc' 
 PaytAuthsnCreditCheckSts_Text : abap.char( 20 ) ; 
 PaytAuthsnCreditCheckSts_Te_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
