/********** GENERATED on 09/05/2021 at 08:55:01 by CB9980000028**************/
 @OData.entitySet.name: 'I_HdrBillgIncompletionStatusT' 
 @OData.entityType.name: 'I_HdrBillgIncompletionStatusTType' 
 define root abstract entity ZI_HDRBILLGINCOMPLET5FC294CBA1 { 
 key HeaderBillgIncompletionStatus : abap.char( 1 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'HdrBillgIncompletionStatusD_vc' 
 HdrBillgIncompletionStatusDesc : abap.char( 20 ) ; 
 HdrBillgIncompletionStatusD_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
