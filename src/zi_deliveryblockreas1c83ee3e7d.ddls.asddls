/********** GENERATED on 09/05/2021 at 08:54:58 by CB9980000028**************/
 @OData.entitySet.name: 'I_DeliveryBlockReason' 
 @OData.entityType.name: 'I_DeliveryBlockReasonType' 
 define root abstract entity ZI_DELIVERYBLOCKREAS1C83EE3E7D { 
 key DeliveryBlockReason : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'DeliveryBlockReason_Text_vc' 
 DeliveryBlockReason_Text : abap.char( 20 ) ; 
 DeliveryBlockReason_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'DeliveryDueListBlock_vc' 
 DeliveryDueListBlock : abap_boolean ; 
 DeliveryDueListBlock_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
