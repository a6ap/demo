/********** GENERATED on 09/05/2021 at 08:55:04 by CB9980000028**************/
 @OData.entitySet.name: 'I_IssueCategoryText' 
 @OData.entityType.name: 'I_IssueCategoryTextType' 
 define root abstract entity ZI_ISSUECATEGORYTEXT915066CF63 { 
 key IssueCategory : abap.char( 5 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'IssueCategoryName_vc' 
 IssueCategoryName : abap.char( 60 ) ; 
 IssueCategoryName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
