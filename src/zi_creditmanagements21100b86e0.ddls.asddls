/********** GENERATED on 09/05/2021 at 08:54:55 by CB9980000028**************/
 @OData.entitySet.name: 'I_CreditManagementSegment' 
 @OData.entityType.name: 'I_CreditManagementSegmentType' 
 define root abstract entity ZI_CREDITMANAGEMENTS21100B86E0 { 
 key CreditSegment : abap.char( 10 ) ; 
 @Odata.property.valueControl: 'CreditSegment_Text_vc' 
 CreditSegment_Text : abap.char( 50 ) ; 
 CreditSegment_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CreditSegmentCurrency_vc' 
 CreditSegmentCurrency : abap.char( 5 ) ; 
 CreditSegmentCurrency_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
