/********** GENERATED on 09/05/2021 at 08:55:07 by CB9980000028**************/
 @OData.entitySet.name: 'I_SalesDocumentRjcnReason' 
 @OData.entityType.name: 'I_SalesDocumentRjcnReasonType' 
 define root abstract entity ZI_SALESDOCUMENTRJCN65C0D3DDB5 { 
 key SalesDocumentRjcnReason : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'SalesDocumentRjcnReason_Tex_vc' 
 SalesDocumentRjcnReason_Text : abap.char( 40 ) ; 
 SalesDocumentRjcnReason_Tex_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
