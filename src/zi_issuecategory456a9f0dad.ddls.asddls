/********** GENERATED on 09/05/2021 at 08:55:03 by CB9980000028**************/
 @OData.entitySet.name: 'I_IssueCategory' 
 @OData.entityType.name: 'I_IssueCategoryType' 
 define root abstract entity ZI_ISSUECATEGORY456A9F0DAD { 
 key IssueCategory : abap.char( 5 ) ; 
 @Odata.property.valueControl: 'IssueCategory_Text_vc' 
 IssueCategory_Text : abap.char( 60 ) ; 
 IssueCategory_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
