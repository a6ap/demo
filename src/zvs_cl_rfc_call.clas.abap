CLASS zvs_cl_rfc_call DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS zvs_cl_rfc_call IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    CONSTANTS lc_destination_name TYPE string VALUE 'S4B_SYSTEM_RFC'.
    TRY.
        DATA(lo_destination) = cl_rfc_destination_provider=>create_by_cloud_destination( i_name = lc_destination_name ).

        DATA(lv_destination) = lo_destination->get_destination_name( ).
        DATA lv_result TYPE c LENGTH 200.

        CALL FUNCTION 'RFC_SYSTEM_INFO'
          DESTINATION lv_destination
          IMPORTING
            rfcsi_export = lv_result.

        out->write( lv_result ).
      CATCH cx_root INTO DATA(lx_error).
        out->write( lx_error->get_text( ) ).
    ENDTRY.

  ENDMETHOD.
ENDCLASS.
