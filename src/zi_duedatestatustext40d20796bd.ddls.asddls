/********** GENERATED on 09/05/2021 at 08:54:59 by CB9980000028**************/
 @OData.entitySet.name: 'I_DueDateStatusText' 
 @OData.entityType.name: 'I_DueDateStatusTextType' 
 define root abstract entity ZI_DUEDATESTATUSTEXT40D20796BD { 
 key DueDateStatus : abap.char( 1 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'DueDateStatusDesc_vc' 
 DueDateStatusDesc : abap.char( 60 ) ; 
 DueDateStatusDesc_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
