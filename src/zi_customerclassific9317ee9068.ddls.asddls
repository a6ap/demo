/********** GENERATED on 09/05/2021 at 08:54:57 by CB9980000028**************/
 @OData.entitySet.name: 'I_CustomerClassificationText' 
 @OData.entityType.name: 'I_CustomerClassificationTextType' 
 define root abstract entity ZI_CUSTOMERCLASSIFIC9317EE9068 { 
 key CustomerClassification : abap.char( 2 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'CustomerClassificationDesc_vc' 
 CustomerClassificationDesc : abap.char( 20 ) ; 
 CustomerClassificationDesc_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
