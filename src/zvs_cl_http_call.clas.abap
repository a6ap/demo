CLASS zvs_cl_http_call DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS zvs_cl_http_call IMPLEMENTATION.
  METHOD if_oo_adt_classrun~main.
    CONSTANTS lc_destination_name TYPE string VALUE 'S4B_SYSTEM_HTTP'.
    TRY.
        DATA(lo_destination) = cl_http_destination_provider=>create_by_cloud_destination(
          i_name = lc_destination_name
          i_authn_mode = if_a4c_cp_service=>service_specific
        ).
        "Destination by URL like below will work
        "DATA(lo_destination) = cl_http_destination_provider=>create_by_url(
        "  i_url = 'http://google.com'
        ").

        DATA(lo_client) = cl_web_http_client_manager=>create_by_http_destination( lo_destination ).
        DATA(lo_request) = lo_client->get_http_request( ).
        lo_request->set_header_fields( VALUE #(
          ( name = 'Accept' value = 'application/json' )
          ( name = 'APIKey' value = 'JGh41gOUhwMaYsf0aBRkt6BN6W6grAQ4' ) )
        ).
        DATA(lo_response) = lo_client->execute( i_method = if_web_http_client=>get ).

        out->write( lo_response->get_text( ) ).
      CATCH cx_root INTO DATA(lx_error).
        out->write( lx_error->get_text( ) ).
    ENDTRY.
  ENDMETHOD.
ENDCLASS.
