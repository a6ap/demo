/********** GENERATED on 09/05/2021 at 08:54:54 by CB9980000028**************/
 @OData.entitySet.name: 'I_BillingBlockReasonText' 
 @OData.entityType.name: 'I_BillingBlockReasonTextType' 
 define root abstract entity ZI_BILLINGBLOCKREASO1D2F20BE82 { 
 key BillingBlockReason : abap.char( 2 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'BillingBlockReasonDescripti_vc' 
 BillingBlockReasonDescription : abap.char( 20 ) ; 
 BillingBlockReasonDescripti_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
