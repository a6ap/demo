/********** GENERATED on 09/05/2021 at 08:55:06 by CB9980000028**************/
 @OData.entitySet.name: 'I_OvrlItmDelivIncompletionStsT' 
 @OData.entityType.name: 'I_OvrlItmDelivIncompletionStsTType' 
 define root abstract entity ZI_OVRLITMDELIVINCOM132FBA0E55 { 
 key OvrlItmDelivIncompletionSts : abap.char( 1 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'OvrlItmDelivIncompltnStsDes_vc' 
 OvrlItmDelivIncompltnStsDesc : abap.char( 20 ) ; 
 OvrlItmDelivIncompltnStsDes_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
