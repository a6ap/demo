/********** GENERATED on 09/05/2021 at 08:55:10 by CB9980000028**************/
 @OData.entitySet.name: 'I_SDDocumentCategory' 
 @OData.entityType.name: 'I_SDDocumentCategoryType' 
 define root abstract entity ZI_SDDOCUMENTCATEGORFACA8E170A { 
 key SDDocumentCategory : abap.char( 4 ) ; 
 @Odata.property.valueControl: 'SDDocumentCategory_Text_vc' 
 SDDocumentCategory_Text : abap.char( 60 ) ; 
 SDDocumentCategory_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
