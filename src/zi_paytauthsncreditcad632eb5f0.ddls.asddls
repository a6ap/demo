/********** GENERATED on 09/05/2021 at 08:55:07 by CB9980000028**************/
 @OData.entitySet.name: 'I_PaytAuthsnCreditCheckStsText' 
 @OData.entityType.name: 'I_PaytAuthsnCreditCheckStsTextType' 
 define root abstract entity ZI_PAYTAUTHSNCREDITCAD632EB5F0 { 
 key PaytAuthsnCreditCheckSts : abap.char( 1 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'PaytAuthsnCreditCheckStsDes_vc' 
 PaytAuthsnCreditCheckStsDesc : abap.char( 20 ) ; 
 PaytAuthsnCreditCheckStsDes_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
