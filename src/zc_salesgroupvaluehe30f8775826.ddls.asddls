/********** GENERATED on 09/05/2021 at 08:54:53 by CB9980000028**************/
 @OData.entitySet.name: 'C_SalesGroupValueHelp' 
 @OData.entityType.name: 'C_SalesGroupValueHelpType' 
 define root abstract entity ZC_SALESGROUPVALUEHE30F8775826 { 
 key SalesOffice : abap.char( 4 ) ; 
 key SalesGroup : abap.char( 3 ) ; 
 @Odata.property.valueControl: 'SalesGroup_Text_vc' 
 SalesGroup_Text : abap.char( 20 ) ; 
 SalesGroup_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
