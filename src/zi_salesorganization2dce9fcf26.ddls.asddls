/********** GENERATED on 09/05/2021 at 08:55:09 by CB9980000028**************/
 @OData.entitySet.name: 'I_SalesOrganizationText' 
 @OData.entityType.name: 'I_SalesOrganizationTextType' 
 define root abstract entity ZI_SALESORGANIZATION2DCE9FCF26 { 
 key SalesOrganization : abap.char( 4 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'SalesOrganizationName_vc' 
 SalesOrganizationName : abap.char( 20 ) ; 
 SalesOrganizationName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
