/********** GENERATED on 09/05/2021 at 08:55:08 by CB9980000028**************/
 @OData.entitySet.name: 'I_SalesDocumentType' 
 @OData.entityType.name: 'I_SalesDocumentTypeType' 
 define root abstract entity ZI_SALESDOCUMENTTYPEE13774EFBE { 
 key SalesDocumentType : abap.char( 4 ) ; 
 @Odata.property.valueControl: 'SalesDocumentType_Text_vc' 
 SalesDocumentType_Text : abap.char( 20 ) ; 
 SalesDocumentType_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SDDocumentCategory_vc' 
 SDDocumentCategory : abap.char( 4 ) ; 
 SDDocumentCategory_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'SalesDocumentProcessingType_vc' 
 SalesDocumentProcessingType : abap.char( 1 ) ; 
 SalesDocumentProcessingType_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'ScreenSequenceGroup_vc' 
 ScreenSequenceGroup : abap.char( 4 ) ; 
 ScreenSequenceGroup_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OrderTypeForBillingRequest_vc' 
 OrderTypeForBillingRequest : abap.char( 4 ) ; 
 OrderTypeForBillingRequest_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'RetsMgmtIsActive_vc' 
 RetsMgmtIsActive : abap.char( 1 ) ; 
 RetsMgmtIsActive_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
