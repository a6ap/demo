/********** GENERATED on 09/05/2021 at 08:54:58 by CB9980000028**************/
 @OData.entitySet.name: 'I_DeliveryBlockReasonText' 
 @OData.entityType.name: 'I_DeliveryBlockReasonTextType' 
 define root abstract entity ZI_DELIVERYBLOCKREASFF66B3798F { 
 key DeliveryBlockReason : abap.char( 2 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'DeliveryBlockReasonText_vc' 
 DeliveryBlockReasonText : abap.char( 20 ) ; 
 DeliveryBlockReasonText_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
