/********** GENERATED on 09/05/2021 at 08:54:56 by CB9980000028**************/
 @OData.entitySet.name: 'I_Currency' 
 @OData.entityType.name: 'I_CurrencyType' 
 define root abstract entity ZI_CURRENCYE5F35E6075 { 
 key Currency : abap.char( 5 ) ; 
 @Odata.property.valueControl: 'Currency_Text_vc' 
 Currency_Text : abap.char( 40 ) ; 
 Currency_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'Decimals_vc' 
 Decimals : abap.int1 ; 
 Decimals_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CurrencyISOCode_vc' 
 CurrencyISOCode : abap.char( 3 ) ; 
 CurrencyISOCode_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'AlternativeCurrencyKey_vc' 
 AlternativeCurrencyKey : abap.char( 3 ) ; 
 AlternativeCurrencyKey_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'IsPrimaryCurrencyForISOCrcy_vc' 
 IsPrimaryCurrencyForISOCrcy : abap_boolean ; 
 IsPrimaryCurrencyForISOCrcy_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
