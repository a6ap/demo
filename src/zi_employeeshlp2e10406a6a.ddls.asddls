/********** GENERATED on 09/05/2021 at 08:55:00 by CB9980000028**************/
 @OData.entitySet.name: 'I_Employeeshlp' 
 @OData.entityType.name: 'I_EmployeeshlpType' 
 define root abstract entity ZI_EMPLOYEESHLP2E10406A6A { 
 key PersonnelNumber : abap.numc( 8 ) ; 
 @Odata.property.valueControl: 'FamilyName_vc' 
 FamilyName : abap.char( 35 ) ; 
 FamilyName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'GivenName_vc' 
 GivenName : abap.char( 35 ) ; 
 GivenName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'Employee_vc' 
 Employee : abap.char( 60 ) ; 
 Employee_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'EmployeeFullName_vc' 
 EmployeeFullName : abap.char( 80 ) ; 
 EmployeeFullName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'UserID_vc' 
 UserID : abap.char( 12 ) ; 
 UserID_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'AuthorizationGroup_vc' 
 AuthorizationGroup : abap.char( 4 ) ; 
 AuthorizationGroup_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'IsBusinessPurposeCompleted_vc' 
 IsBusinessPurposeCompleted : abap.char( 1 ) ; 
 IsBusinessPurposeCompleted_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
