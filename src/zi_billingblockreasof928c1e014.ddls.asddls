/********** GENERATED on 09/05/2021 at 08:54:54 by CB9980000028**************/
 @OData.entitySet.name: 'I_BillingBlockReason' 
 @OData.entityType.name: 'I_BillingBlockReasonType' 
 define root abstract entity ZI_BILLINGBLOCKREASOF928C1E014 { 
 key BillingBlockReason : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'BillingBlockReason_Text_vc' 
 BillingBlockReason_Text : abap.char( 20 ) ; 
 BillingBlockReason_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
