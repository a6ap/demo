/********** GENERATED on 09/05/2021 at 08:54:59 by CB9980000028**************/
 @OData.entitySet.name: 'I_DueDateStatus' 
 @OData.entityType.name: 'I_DueDateStatusType' 
 define root abstract entity ZI_DUEDATESTATUS25B71FA332 { 
 key DueDateStatus : abap.char( 1 ) ; 
 @Odata.property.valueControl: 'DueDateStatus_Text_vc' 
 DueDateStatus_Text : abap.char( 60 ) ; 
 DueDateStatus_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
