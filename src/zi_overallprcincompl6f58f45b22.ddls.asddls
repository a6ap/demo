/********** GENERATED on 09/05/2021 at 08:55:05 by CB9980000028**************/
 @OData.entitySet.name: 'I_OverallPrcIncompletionStsT' 
 @OData.entityType.name: 'I_OverallPrcIncompletionStsTType' 
 define root abstract entity ZI_OVERALLPRCINCOMPL6F58F45B22 { 
 key OverallPricingIncompletionSts : abap.char( 1 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'OverallPrcIncompletionStsDe_vc' 
 OverallPrcIncompletionStsDesc : abap.char( 20 ) ; 
 OverallPrcIncompletionStsDe_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
