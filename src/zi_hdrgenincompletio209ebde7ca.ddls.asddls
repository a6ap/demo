/********** GENERATED on 09/05/2021 at 08:55:02 by CB9980000028**************/
 @OData.entitySet.name: 'I_HdrGenIncompletionStatusT' 
 @OData.entityType.name: 'I_HdrGenIncompletionStatusTType' 
 define root abstract entity ZI_HDRGENINCOMPLETIO209EBDE7CA { 
 key HdrGeneralIncompletionStatus : abap.char( 1 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'HdrGenIncompletionStatusDes_vc' 
 HdrGenIncompletionStatusDesc : abap.char( 20 ) ; 
 HdrGenIncompletionStatusDes_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
