/********** GENERATED on 09/05/2021 at 08:54:53 by CB9980000028**************/
 @OData.entitySet.name: 'C_OrgDivisionValueHelp' 
 @OData.entityType.name: 'C_OrgDivisionValueHelpType' 
 define root abstract entity ZC_ORGDIVISIONVALUEH37D1858C92 { 
 key SalesOrganization : abap.char( 4 ) ; 
 key DistributionChannel : abap.char( 2 ) ; 
 key Division : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'Division_Text_vc' 
 Division_Text : abap.char( 20 ) ; 
 Division_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
