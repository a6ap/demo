/********** GENERATED on 09/05/2021 at 08:54:53 by CB9980000028**************/
 @OData.entitySet.name: 'C_SalesOfficeValueHelp' 
 @OData.entityType.name: 'C_SalesOfficeValueHelpType' 
 define root abstract entity ZC_SALESOFFICEVALUEH510468EAF3 { 
 key SalesOrganization : abap.char( 4 ) ; 
 key DistributionChannel : abap.char( 2 ) ; 
 key OrganizationDivision : abap.char( 2 ) ; 
 key SalesOffice : abap.char( 4 ) ; 
 @Odata.property.valueControl: 'SalesOffice_Text_vc' 
 SalesOffice_Text : abap.char( 20 ) ; 
 SalesOffice_Text_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
