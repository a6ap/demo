/********** GENERATED on 09/05/2021 at 08:55:09 by CB9980000028**************/
 @OData.entitySet.name: 'I_SalesOfficeText' 
 @OData.entityType.name: 'I_SalesOfficeTextType' 
 define root abstract entity ZI_SALESOFFICETEXTB8E134E93E { 
 key SalesOffice : abap.char( 4 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'SalesOfficeName_vc' 
 SalesOfficeName : abap.char( 20 ) ; 
 SalesOfficeName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
