/********** GENERATED on 09/05/2021 at 08:54:57 by CB9980000028**************/
 @OData.entitySet.name: 'I_Customer_VH' 
 @OData.entityType.name: 'I_Customer_VHType' 
 define root abstract entity ZI_CUSTOMER_VH70AEEAA5D3 { 
 key Customer : abap.char( 10 ) ; 
 @Odata.property.valueControl: 'OrganizationBPName1_vc' 
 OrganizationBPName1 : abap.char( 35 ) ; 
 OrganizationBPName1_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'OrganizationBPName2_vc' 
 OrganizationBPName2 : abap.char( 35 ) ; 
 OrganizationBPName2_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'Country_vc' 
 Country : abap.char( 3 ) ; 
 Country_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CityName_vc' 
 CityName : abap.char( 35 ) ; 
 CityName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'StreetName_vc' 
 StreetName : abap.char( 35 ) ; 
 StreetName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CustomerName_vc' 
 CustomerName : abap.char( 80 ) ; 
 CustomerName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'CustomerAccountGroup_vc' 
 CustomerAccountGroup : abap.char( 4 ) ; 
 CustomerAccountGroup_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'AuthorizationGroup_vc' 
 AuthorizationGroup : abap.char( 4 ) ; 
 AuthorizationGroup_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 @Odata.property.valueControl: 'IsBusinessPurposeCompleted_vc' 
 IsBusinessPurposeCompleted : abap.char( 1 ) ; 
 IsBusinessPurposeCompleted_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
