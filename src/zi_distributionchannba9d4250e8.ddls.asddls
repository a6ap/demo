/********** GENERATED on 09/05/2021 at 08:54:58 by CB9980000028**************/
 @OData.entitySet.name: 'I_DistributionChannelText' 
 @OData.entityType.name: 'I_DistributionChannelTextType' 
 define root abstract entity ZI_DISTRIBUTIONCHANNBA9D4250E8 { 
 key DistributionChannel : abap.char( 2 ) ; 
 key Language : abap.char( 2 ) ; 
 @Odata.property.valueControl: 'DistributionChannelName_vc' 
 DistributionChannelName : abap.char( 20 ) ; 
 DistributionChannelName_vc : RAP_CP_ODATA_VALUE_CONTROL ; 
 
 } 
